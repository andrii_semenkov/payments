package com.payment.core.domain;

import com.payment.core.db.Account;
import com.payment.core.db.AccountDao;
import com.payment.core.db.Payment;
import com.payment.core.db.PaymentDao;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;

@Slf4j
public final class PaymentService {
    private final AccountDao accountDao;
    private final PaymentDao paymentDao;

    private PaymentService(AccountDao accountDao, PaymentDao paymentDao) {
        this.accountDao = accountDao;
        this.paymentDao = paymentDao;
    }

    public static PaymentService create() {
        return new PaymentService(
                AccountDao.create(),
                PaymentDao.create()
        );
    }

    public Payment send(final Account from, final Account to, final BigDecimal amount) {
        log.info("Start sending payment form{} to{}", from, to);
        var payment = new Payment();
        adjustBalances(from, to, amount);
        payment.setAccountFrom(from);
        payment.setAccountTo(to);
        payment.setAmount(amount);
        var sent = paymentDao.save(payment);
        log.info("Payment{} sent.", sent);
        return sent;
    }


    private void adjustBalances(Account from, Account to, BigDecimal amount) {
        from.setBalance(from.getBalance().add(amount.negate()));
        to.setBalance(to.getBalance().add(amount));
        accountDao.save(from);
        accountDao.save(to);
    }

    public History showHistory(final Account account) {
        log.info("Generating history for {}", account);
        return History.of(account, paymentDao.findByAccount(account));
    }
}
