package com.payment.core.domain;

import com.payment.core.db.Account;
import com.payment.core.db.Payment;
import lombok.Value;

import java.util.List;
import java.util.stream.Collectors;

@Value
public final class History {
    private final Account account;
    private final List<Payment> inbound;
    private final List<Payment> outbound;

    private History(Account account, List<Payment> inbound, List<Payment> outbound) {
        this.account = account;
        this.inbound = inbound;
        this.outbound = outbound;
    }

    static History of(final Account account, final List<Payment> payments) {
        var outbound = payments.stream()
                .filter(payment -> payment.getAccountFrom().getId().equals(account.getId()) &&
                        payment.getAccountFrom().getBalance().compareTo(account.getBalance()) == 0)
                .collect(Collectors.toList());
        var inbound = payments.stream()
                .filter(payment -> payment.getAccountTo().getId().equals(account.getId()) &&
                        payment.getAccountTo().getBalance().compareTo(account.getBalance()) == 0)
                .collect(Collectors.toList());
        return new History(account, inbound, outbound);
    }
}
