package com.payment.core.api;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.payment.core.db.Account;
import com.payment.core.db.AccountDao;
import com.payment.core.db.Payment;
import com.payment.core.db.PaymentDao;
import com.payment.core.domain.History;
import com.payment.core.domain.PaymentService;
import lombok.extern.slf4j.Slf4j;
import spark.Request;

import java.math.BigDecimal;

import static com.payment.core.db.Account.emptyAccount;
import static org.apache.commons.lang3.math.NumberUtils.isParsable;
import static spark.Spark.*;

@Slf4j
public final class PaymentController {
    private static final AccountDao accountDao = AccountDao.create();
    private static final PaymentDao paymentDao = PaymentDao.create();
    private static final PaymentService paymentService = PaymentService.create();

    public static void init() {
        path("api/", () -> {
            post("/send", (req, res) -> {
                log.debug("Request {}; Response {}", req, res);
                var dto = readDto(req);
                log.debug("Transformed {}", dto);
                res.body(executePayment(dto));
                return res;
            });
            get("/history/:userId", (req, res) -> {
                String userId = req.params("userId");
                if (isParsable(userId)) {
                    var json = accountDao.findByAccountNumber(Long.valueOf(userId))
                            .stream()
                            .map(paymentService::showHistory)
                            .map(PaymentController::mapToJson)
                            .findFirst();
                    res.body(json.orElseThrow());
                } else {
                    res.status(400);
                }
                return res;
            });
        });
    }

    private static String mapToJson(final History history) {
        try {
            return new ObjectMapper().writeValueAsString(history);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        log.error("History not mapped properly: {}", history);
        return null;
    }

    private static void createAccountIfNotExist(final Account from, final Account to) {
        //Create user adhoc for testing purpose
        if (accountDao.findByAccountNumber(from.getAccountNumber()).isEmpty()) {
            from.setBalance(BigDecimal.valueOf(10000));
            accountDao.save(from);
        }
        if (accountDao.findByAccountNumber(to.getAccountNumber()).isEmpty()) {
            to.setBalance(BigDecimal.valueOf(10000));
            accountDao.save(to);
        }
    }


    private static PaymentDTO readDto(final Request req) throws java.io.IOException {
        return new ObjectMapper().readValue(req.body(), PaymentDTO.class);
    }

    private static String executePayment(final PaymentDTO dto) {
        createAccountIfNotExist(emptyAccount(dto.getAccountFromId()), emptyAccount(dto.getAccountToId()));
        Payment send = paymentService.send(findAccount(dto.getAccountFromId()),
                findAccount(dto.getAccountToId()),
                dto.getAmount());
        return String.format("Payment done, your balance %s", send.getAccountFrom().getBalance());
    }

    private static Account findAccount(Long accountFromId) {
        return accountDao.find(accountFromId).orElseThrow();
    }
}
