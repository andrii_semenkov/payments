package com.payment.core.api;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

import java.math.BigDecimal;

@Value
public class PaymentDTO {
    private final Long accountFromId;
    private final Long accountToId;
    private final BigDecimal amount;

    @JsonCreator(mode = JsonCreator.Mode.PROPERTIES)
    public PaymentDTO(@JsonProperty("accountFromId") Long accountFromId,
                      @JsonProperty("accountToId") Long accountToId,
                      @JsonProperty("amount") BigDecimal amount) {
        this.accountFromId = accountFromId;
        this.accountToId = accountToId;
        this.amount = amount;
    }
}
