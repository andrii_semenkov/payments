package com.payment.core.api;

import lombok.Value;

import java.util.List;

@Value
public class HistoryDTO {
    private final Long accountId;
    private final List<PaymentDTO> inbound;
    private final List<PaymentDTO> outbound;
}
