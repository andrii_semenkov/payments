package com.payment.core;

import com.payment.core.api.PaymentController;
import lombok.extern.slf4j.Slf4j;

import static spark.Spark.port;

/**
 * Main application entry-point.
 */
@Slf4j
public class PaymentApp {
    public static void main(String... args) {
        serverConfiguration();
        PaymentController.init();
    }

    private static void serverConfiguration() {
        port(8080);
    }
}
