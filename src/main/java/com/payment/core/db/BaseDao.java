package com.payment.core.db;

import lombok.extern.slf4j.Slf4j;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import java.util.Optional;

import static java.util.Objects.nonNull;

@Slf4j
public abstract class BaseDao<T extends BaseEntity> implements Dao<T, Long> {
    private final Class<T> entityClass;
    protected final EntityManager entityManager;

    protected BaseDao(final Class<T> entityClass) {
        this.entityClass = entityClass;
        entityManager = HibernateUtil.getEntityManager();
    }

    @Override
    public T save(T entity) {
        log.info("Saving entity {}", entity);
        EntityTransaction transaction = getTransaction();
        transaction.begin();
        entityManager.persist(entity);
        Long id = entity.getId();
        if (nonNull(id)) {
            T t = find(id).orElseThrow();
            transaction.commit();
            return t;
        } else {
            log.error("Entity:{}", entity);
            throw new IllegalStateException("Entity not saved.");
        }
    }

    @Override
    public Optional<T> find(Long o) {
        T o1 = entityManager.find(entityClass, o);
        log.info("Finding entity {} by id {}", o1, o);
        return Optional.ofNullable(o1);
    }

    @Override
    public void remove(T entity) {
        log.info("Removing entity {}", entity);
        getTransaction().begin();
        entityManager.remove(entity);
        getTransaction().commit();
    }

    private EntityTransaction getTransaction() {
        return entityManager.getTransaction();
    }
}
