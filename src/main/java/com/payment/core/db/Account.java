package com.payment.core.db;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;


@Data
@Table(name = Account.NAME)
@Entity
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Account extends BaseEntity {
    public static final String NAME = "account";
    @NotNull
    @Column(precision = 19, scale = 2)
    private BigDecimal balance;
    @NotNull
    private Long accountNumber;

    public static Account emptyAccount(Long accountNumber) {
        Account account = new Account();
        account.setAccountNumber(accountNumber);
        return account;
    }
}
