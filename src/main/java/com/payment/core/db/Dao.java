package com.payment.core.db;

import java.util.Optional;

public interface Dao<T, ID> {
    T save(T entity);

    Optional<T> find(ID id);

    void remove(T entity);
}
