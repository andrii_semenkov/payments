package com.payment.core.db;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;


@Data
@Entity
@Table
@Builder
@NoArgsConstructor
@RequiredArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Payment extends BaseEntity {
    @NotNull
    @NonNull
    @ManyToOne(optional = false)
    private Account accountFrom;
    @NotNull
    @NonNull
    @ManyToOne(optional = false)
    private Account accountTo;
    @NonNull
    @NotNull
    private BigDecimal amount;
}
