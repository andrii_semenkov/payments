package com.payment.core.db;

import java.util.Optional;

public class AccountDao extends BaseDao<Account> {
    private AccountDao() {
        super(Account.class);
    }

    public static AccountDao create() {
        return new AccountDao();
    }

    public Optional<Account> findByAccountNumber(Long accountNumber) {
        return this.entityManager.createQuery("from Account where accountNumber = " + accountNumber, Account.class)
                .getResultList()
                .stream()
                .findFirst();
    }

    public boolean exists(final Account account) {
        if (account.getId() == null) {
            return false;
        }
        boolean exist = Optional
                .ofNullable(this.entityManager.find(Account.class, account.getId()))
                .isPresent();
        return exist;
    }
}
