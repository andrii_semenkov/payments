package com.payment.core.db;

import java.util.List;

public class PaymentDao extends BaseDao<Payment> {
    private PaymentDao() {
        super(Payment.class);
    }

    public static PaymentDao create() {
        return new PaymentDao();
    }

    public List<Payment> findByAccount(final Account account) {
        return this.entityManager.createQuery(
                "from Payment where accountFrom = :account or accountTo = :account",
                Payment.class
        ).setParameter("account", account)
                .getResultList();
    }
}
