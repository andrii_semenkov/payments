package com.payment.core.env;

import com.payment.core.PaymentApp;
import org.junit.BeforeClass;
import spark.Spark;

public abstract class BaseIntegrationTest {
    private static Boolean executed = false;

    @BeforeClass
    public static synchronized void setup() throws InterruptedException {
        if (!executed) {
            executed = true;
            PaymentApp.main("");
            Spark.awaitInitialization();
        }
    }

    public String getServerURI() {
        return "http://localhost:8080/";
    }
}
