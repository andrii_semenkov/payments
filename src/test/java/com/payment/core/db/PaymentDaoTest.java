package com.payment.core.db;

import com.payment.core.env.BaseIntegrationTest;
import org.junit.Test;

import java.math.BigDecimal;

public class PaymentDaoTest extends BaseIntegrationTest {
    private final PaymentDao dao = PaymentDao.create();
    private final AccountDao accountDao = AccountDao.create();

    @Test
    public void should_save() {
        var payment = Payment.builder()
                .amount(BigDecimal.valueOf(20L))
                .accountFrom(accountDao.save(new Account()))
                .accountTo(accountDao.save(new Account()))
                .build();
        assert dao.save(payment) != null;
    }

}