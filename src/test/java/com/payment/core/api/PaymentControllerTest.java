package com.payment.core.api;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.payment.core.env.BaseIntegrationTest;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.junit.Test;

import java.io.IOException;
import java.math.BigDecimal;

public class PaymentControllerTest extends BaseIntegrationTest {

    @Test
    public void should_make_payment() throws IOException {
        HttpResponse response = doPost();

        assert response.getStatusLine().getStatusCode() == 200;
    }

    private HttpResponse doPost() throws IOException {
        var request = new HttpPost(getServerURI() + "api/send");
        var paymentDTO = new PaymentDTO(1L, 2L, BigDecimal.valueOf(25L));
        request.setEntity(new StringEntity(mapToString(paymentDTO)));


        return doRequest(request);
    }

    @Test
    public void should_show_history() throws IOException {
        doPost();

        HttpUriRequest request = new HttpGet(getServerURI() + "api/history/1");

        // When
        HttpResponse response = doRequest(request);

        // Then
        assert response.getStatusLine().getStatusCode() == 200;
    }

    private HttpResponse doRequest(HttpUriRequest request) throws IOException {
        return HttpClientBuilder.create()
                .build()
                .execute(request);
    }

    private String mapToString(PaymentDTO paymentDTO) throws JsonProcessingException {
        return new ObjectMapper().writeValueAsString(paymentDTO);
    }


}